function createTable(n) {
  const table = document.createElement('table');
  const tableTimes = document.createElement('tr');
  const tableLine = document.createElement('tr');

  tableTimes.innerHTML = '&times';
  document.body.appendChild(table);
  tableLine.appendChild(tableTimes);
  table.appendChild(tableLine);

  tableTimes.className = 'cell header';
  for (let i = 0; i <= n; i++) {
    const row = document.createElement('th');
    row.innerHTML = i;
    tableLine.appendChild(row);
    row.className = 'cell header';
  }
  for (let j = 0; j <= n; j++) {
    const col = document.createElement('tr');
    col.innerHTML = j;
    col.className = 'header cell';
    table.appendChild(col);
    for (let i = 0; i <= n; i++) {
      const result = document.createElement('td');
      result.innerHTML = i * j;
      col.appendChild(result);
      result.className = 'cell result';
    }
  }
}
document.getElementById('submit').addEventListener('click', () => {
  value = document.getElementById('maxValue').value;
  createTable(value);
});

document.getElementById('submitColor').addEventListener('click', () => {
  const colorChange = document.querySelectorAll('.result');
  value = document.getElementById('color').value;
  for (let i = 0; i < colorChange.length; i++) {
    colorChange[i].style.backgroundColor = value;
  }
});

document.getElementById('submitColorHeader').addEventListener('click', () => {
  const colorChangeHeader = document.querySelectorAll('.header');
  value = document.getElementById('colorHeader').value;
  for (let i = 0; i < colorChangeHeader.length; i++) {
    colorChangeHeader[i].style.backgroundColor = value;
  }
});
